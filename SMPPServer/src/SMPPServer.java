
import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.commons.util.windowing.WindowFuture;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppServerConfiguration;
import com.cloudhopper.smpp.SmppServerHandler;
import com.cloudhopper.smpp.SmppServerSession;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.impl.DefaultSmppServer;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.BaseBind;
import com.cloudhopper.smpp.pdu.BaseBindResp;
import com.cloudhopper.smpp.pdu.DeliverSm;
import com.cloudhopper.smpp.pdu.DeliverSmResp;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.type.Address;
import com.cloudhopper.smpp.type.SmppProcessingException;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SMPPServer {
	private static final Logger logger = LoggerFactory.getLogger(SMPPServer.class);

	static public void main(String[] args) throws Exception {

		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

		ScheduledThreadPoolExecutor monitorExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1,
				new ThreadFactory() {
					private AtomicInteger sequence = new AtomicInteger(0);

					@Override
					public Thread newThread(Runnable r) {
						Thread t = new Thread(r);
						t.setName("============= SmppServerSessionWindowMonitorPool-" + sequence.getAndIncrement());
						return t;
					}
				});

		SmppServerConfiguration configuration = new SmppServerConfiguration();
		configuration.setPort(2776);
		configuration.setMaxConnectionSize(10);
		configuration.setNonBlockingSocketsEnabled(true);
		configuration.setDefaultRequestExpiryTimeout(30000);
		configuration.setDefaultWindowMonitorInterval(15000);
		configuration.setDefaultWindowSize(5);
		configuration.setDefaultWindowWaitTimeout(configuration.getDefaultRequestExpiryTimeout());
		configuration.setDefaultSessionCountersEnabled(true);
		configuration.setJmxEnabled(true);

		DefaultSmppServer smppServer = new DefaultSmppServer(configuration, new DefaultSmppServerHandler());

		logger.info("Starting SMPP server...");
		smppServer.start();
		logger.info("SMPP server started");

		System.out.println("Press any key to stop server");
		System.in.read();

		logger.info("Stopping SMPP server...");
		smppServer.stop();
		logger.info("SMPP server stopped");

		// logger.info("Server counters: {}", smppServer.getCounters());
	}

	public static class DefaultSmppServerHandler implements SmppServerHandler {

		@Override
		public void sessionBindRequested(Long sessionId, SmppSessionConfiguration sessionConfiguration,
				final BaseBind bindRequest) throws SmppProcessingException {

			// sessionConfiguration.setName("Application.SMPP." +
			// sessionConfiguration.getSystemId());
		}

		@Override
		public void sessionCreated(Long sessionId, SmppServerSession session, BaseBindResp preparedBindResponse)
				throws SmppProcessingException {
			logger.info(" = = = = Session created: {}", session);
			session.serverReady(new TestSmppSessionHandler(session));
		}

		@Override
		public void sessionDestroyed(Long sessionId, SmppServerSession session) {
			logger.info("Session destroyed: {}", session);
			if (session.hasCounters()) {
				logger.info(" final session rx-submitSM: {}", session.getCounters().getRxSubmitSM());

			}

			session.destroy();
		}
	}

	public static class TestSmppSessionHandler extends DefaultSmppSessionHandler {

		private WeakReference<SmppSession> sessionRef;

		public TestSmppSessionHandler(SmppSession session) {
			this.sessionRef = new WeakReference<SmppSession>(session);
		}

		@Override
		public PduResponse firePduRequestReceived(PduRequest pduRequest) {
			SmppSession session = sessionRef.get();
			// System.out.println("cm: "+pduRequest.getCommandId());

			if (pduRequest.getCommandId() == SmppConstants.CMD_ID_SUBMIT_SM) {
				SubmitSm sm = (SubmitSm) pduRequest;
				byte[] shortMessage = sm.getShortMessage();
				String SMS = new String(shortMessage);
				System.out.println("Dari : " + sm.getSourceAddress().getAddress());
				System.out.println("Kepada : " + sm.getDestAddress().getAddress());
				System.out.println("Pesan :  " + SMS);

				byte textBytes = 0;
				sm.setRegisteredDelivery(textBytes);
				try {
					
					String text160 = "asdahjgdhaghdjahsgdjhagjhgdhahdvvvvvvvvvvadjjjjakjjsdkajsdkasdkhjakhdakjsdhjkasdhajshdjkauwwwwwwwwwwwwwwwwwwwwwwwwoqqqqqqqqqqqqqqqqqqqwuyeuquuqwudeqhduoqquhwdqouw";
					byte[] textBytes2 = CharsetUtil.encode(text160, CharsetUtil.CHARSET_GSM);

					DeliverSm deliver = new DeliverSm();

					deliver.setSourceAddress(new Address((byte) 0x03, (byte) 0x00, "6287849707098;6287849707098"));
					deliver.setDestAddress(new Address((byte) 0x01, (byte) 0x01, "KlubBundaGSM"));
					deliver.setShortMessage(textBytes2);

					WindowFuture<Integer, PduRequest, PduResponse> future = session.sendRequestPdu(deliver, 10000,
							false);
					
				} catch (Exception e) {
				}
				PduResponse resultPduResponse = pduRequest.createResponse();
				return processDefaultPduResponse(resultPduResponse);
			}

			return pduRequest.createResponse();
		}

		private PduResponse processDefaultPduResponse(PduResponse pduResponse) {
			return pduResponse;
		}
	}

}
